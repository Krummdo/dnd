import React, { Component } from 'react';
import { Container, Draggable } from 'react-smooth-dnd';
import { applyDrag, generateItems } from './utils';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({
	card: {
		minWidth: 275,
		width: theme.spacing.unit,
		margin: '2px'
	},
	bullet: {
		display: 'inline-block',
		margin: '0 2px',
		transform: 'scale(0.8)'
	},
	title: {
		fontSize: 14
	},
	pos: {
		marginBottom: 12
	},
	groupStyle: {
		marginLeft: theme.spacing.unit,
		flex: 1
	}
});

class Mesas extends Component {
	constructor() {
		super();

		this.state = {
			items1: generateItems(10, (i) => ({ id: '1' + i, data: `Grupo 1 - Mesa ${i + 1}` })),
			items2: generateItems(10, (i) => ({ id: '2' + i, data: `Grupo 2 - Mesa ${i + 1}` }))
		};
	}
	render() {
		const { classes } = this.props;

		return (
			<div style={{ display: 'flex', justifyContent: 'stretch', marginTop: '50px', marginRight: '50px' }}>
				<Container
					groupName="1"
					getChildPayload={(i) => this.state.items1[i]}
					onDrop={(e) => this.setState({ items1: applyDrag(this.state.items1, e) })}
				>
					{this.state.items1.map((p) => {
						return (
							<Draggable key={p.id}>
								<Card className={classes.card}>
									<CardContent>
										<Typography className={classes.title} color="textSecondary" gutterBottom>
											Word of the Day
										</Typography>
										<Typography variant="h5" component="h2">
											{p.data}
										</Typography>
										<Typography className={classes.pos} color="textSecondary">
											adjective
										</Typography>
										<Typography component="p">
											well meaning and kindly.
											<br />
											{'"a benevolent smile"'}
										</Typography>
									</CardContent>
									<CardActions>
										<Button size="small">Learn More</Button>
									</CardActions>
								</Card>
							</Draggable>
						);
					})}
				</Container>

				<Container
					groupName="1"
					getChildPayload={(i) => this.state.items2[i]}
					onDrop={(e) => this.setState({ items2: applyDrag(this.state.items2, e) })}
				>
					{this.state.items2.map((p) => {
						return (
							<Draggable key={p.id}>
								<Card className={classes.card}>
									<CardContent>
										<Typography className={classes.title} color="textSecondary" gutterBottom>
											Word of the Day
										</Typography>
										<Typography variant="h5" component="h2">
											{p.data}
										</Typography>
										<Typography className={classes.pos} color="textSecondary">
											adjective
										</Typography>
										<Typography component="p">
											well meaning and kindly.
											<br />
											{'"a benevolent smile"'}
										</Typography>
									</CardContent>
									<CardActions>
										<Button size="small">Learn More</Button>
									</CardActions>
								</Card>
							</Draggable>
						);
					})}
				</Container>
			</div>
		);
	}
}

Mesas.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Mesas);
